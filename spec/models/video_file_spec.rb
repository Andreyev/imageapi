require 'rails_helper'

RSpec.describe VideoFile, type: :model do
  let(:sample_video_file) {File.open Rails.root.join('spec', 'data', 'sample_video.avi')}
  let!(:new_with_file) {
    VideoFile.new do |vf|
      vf.from = 1
      vf.to = 2
      vf.source_video = sample_video_file
      vf.user = User.new(email: 'admin@example.com')
    end
  }

  let(:new_video_file) { VideoFile.new }

  it "has property :enable_processing" do
    expect(new_with_file).to respond_to(:enable_processing)
  end

  it "enable_processing is false by default" do
    expect(new_with_file.enable_processing).to be false
  end

  describe "before_validation" do
    it "calls :source_video_get_duration" do
      expect(new_with_file).to receive :source_video_get_duration
      new_with_file.valid?
    end

    it "calls :source_video_duration=" do
      expect(new_with_file).to receive(:source_video_duration=).and_call_original
      new_with_file.valid?
      expect(new_with_file.source_video_duration.to_i).to eq(50)
    end
  end

  describe "post processing" do
    it "skips when :enable_processing is false" do
      new_with_file.enable_processing = false
      expect(new_with_file.before_source_video_processing).to be false
      expect(new_with_file).to_not receive(:from)
      expect(new_with_file).to_not receive(:to)
      new_with_file.source_video.reprocess!
    end
    it "performs when :enable_processing is true" do
      new_with_file.enable_processing = true
      expect(new_with_file).to receive(:from).and_call_original
      expect(new_with_file).to receive(:to).and_call_original
      new_with_file.source_video.reprocess!
    end
  end

  describe "convert state" do
    it "is updated after altering from" do
      new_video_file.from = 10
      expect(new_video_file).to receive(:convert_state=).with(:new)
      new_video_file.send(:reset_state)
    end
    it "is updated after altering to" do
      new_video_file.to = 10
      expect(new_video_file).to receive(:convert_state=).with(:new)
      new_video_file.send(:reset_state)
    end
    it "is updated after altering source_video" do
      new_video_file.source_video = sample_video_file
      expect(new_video_file).to receive(:convert_state=).with(:new)
      new_video_file.send(:reset_state)
    end
  end

  describe "validates" do
    describe "attachment" do
      describe "on create" do
        it "valid if present" do
          expect(new_with_file).to be_valid
        end
        it "invalid if empty" do
          invalid = VideoFile.new(from: 1, to: 2, user: User.new(email: 'admin@example.com'))
          expect(invalid).to_not be_valid
        end
      end
      describe "on update" do
        before do
          new_with_file.save
        end
        it "valid if file is not updated" do
          new_with_file.from = 2
          new_with_file.to = 3
          expect(new_with_file).to be_valid
        end
      end
    end
  end

end