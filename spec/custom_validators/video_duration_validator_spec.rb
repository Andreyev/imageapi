require 'rails_helper'

RSpec.describe CustomValidators::VideoDurationValidator do
  let(:vf) { VideoFile.new do |vf|
                                vf.from=150
                                vf.to=120
                                vf.source_video_duration=1000
                              end
  }

  let(:good_vf) {
    VideoFile.new do |good_vf|
      good_vf.from = 10
      good_vf.to = 500
      good_vf.source_video_duration = 1000
    end
  }

  describe "invalidates" do
    it "when from > to" do
      subject.validate(vf)
      expect(vf.errors[:from].size).to be > 0
      expect(vf.errors[:to].size).to be > 0
    end
    it "when from out of bounds" do
      vf.from = 1001
      vf.to =1100
      subject.validate(vf)
      expect(vf.errors[:from].size).to be > 0
    end
    it "when to out of bounds" do
      vf.from = 1001
      vf.to =1100
      subject.validate(vf)
      expect(vf.errors[:to].size).to be > 0
    end
    it "when from < 0" do
      vf.from = -10
      vf.to = -5
      subject.validate(vf)
      expect(vf.errors[:from].size).to be > 0
    end
    it "when to < 0" do
      vf.to = -10
      vf.to = -5
      subject.validate(vf)
      expect(vf.errors[:to].size).to be > 0
    end
  end
  describe "pass" do
    it "when from < to" do
      subject.validate good_vf
      expect(good_vf.errors[:from]).to eql []
      expect(good_vf.errors[:to]).to eql []
    end
    it "when from is 0" do
      good_vf.from = 0
      subject.validate good_vf
      expect(good_vf.errors[:from]).to eql []
      expect(good_vf.errors[:to]).to eql []
    end
    it "when to is duration" do
      good_vf.to = 1000
      subject.validate good_vf
      expect(good_vf.errors[:from]).to eql []
      expect(good_vf.errors[:to]).to eql []
    end
  end
end