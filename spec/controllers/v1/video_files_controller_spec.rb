require 'rails_helper'

RSpec.describe V1::VideoFilesController, type: :controller do
  let!(:created_video) { VideoFile.new }
  before do
    allow_any_instance_of(V1::VideoFilesController).to receive(:authenticate_user!).and_return(true)
    allow_any_instance_of(V1::VideoFilesController).to receive(:current_user).and_return(User.new)
    allow_any_instance_of(V1::VideoFilesController).to receive(:set_video_file).and_return(created_video)
    allow_any_instance_of(User).to receive(:changed?).and_return(:false)
    subject.instance_variable_set(:@video_file, created_video)
  end

  it "schedules video processing after create" do
    def subject.create
      respond_with(:v1, @video_file)
    end
    expect(TrimVideoFileJob).to receive(:perform_later).with(created_video)
    post :create, video_file: {bla: :bla}, format: :json
  end

  it "schedules video processing after update" do
    def subject.update
      respond_with(:v1, @video_file)
    end
    expect(TrimVideoFileJob).to receive(:perform_later).with(created_video)
    patch :update, video_file: {bla: :bla}, id: :stubbed, format: :json
  end

  it "schedules video processing by schedule" do
    expect(TrimVideoFileJob).to receive(:perform_later).with(created_video)
    patch :schedule, video_file: {bla: :bla}, id: :stubbed, format: :json
  end

  describe "prevent video scheduling if it is processing" do
    before :each do
      created_video.convert_state = 2
    end

    it "after update" do
      def subject.update
        respond_with(:v1, @video_file)
      end
      expect(TrimVideoFileJob).to_not receive(:perform_later).with(created_video)
      patch :update, video_file: {bla: :bla}, id: :stubbed, format: :json
    end

    it "by schedule" do
      expect(TrimVideoFileJob).to_not receive(:perform_later).with(created_video)
      patch :schedule, video_file: {bla: :bla}, id: :stubbed, format: :json
    end

  end

end
