module CustomValidators
  class VideoDurationValidator < ActiveModel::Validator
    def validate(record)
      if record.source_video_duration.present?
        record.errors.add :from, :out_of_bounds, duration: record.source_video_duration unless record.from >= 0 && record.from < record.source_video_duration
        record.errors.add :to, :out_of_bounds, duration: record.source_video_duration unless record.to <= record.source_video_duration
        if record.from > record.to
          record.errors.add :from, :not_greater_than_to, to: record.to
          record.errors.add :to, :not_less_than_from, from: record.from
        end
      end
    end
  end
end
