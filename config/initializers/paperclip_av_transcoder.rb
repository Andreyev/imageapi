module Av
  module Commands
    class Base
      def parse_param_with_lambda param
        if param.count == 1
          if param[0].is_a? Array
            param[0][1] = param[0][1].call(source_file) if param[0].last.respond_to? :call
            return param[0]
          end
        end
        parse_param_without_lambda param
      end
      alias_method_chain :parse_param, :lambda
    end
  end
end


module Paperclip
  class Transcoder < Processor
    def initialize_with_lambda *args
      initialize_without_lambda *args
      class << @cli.command
        attr_accessor :source_file
      end
      @cli.command.source_file=args[0].instance_variable_get(:@target).try(:instance)
    end
    alias_method_chain :initialize, :lambda
  end
end
