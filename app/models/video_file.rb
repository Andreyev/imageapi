class VideoFile
  CONVERT_STATES = %i(new scheduled processing done failed)
  include Mongoid::Document
  include Mongoid::Paperclip
  include GlobalID::Identification
  attr_accessor :enable_processing

  field :from, type: Float
  field :to, type: Float
  field :source_video_duration
  field :source_video_meta
  field :convert_state, type: Integer, default: 0

  belongs_to :user
  has_mongoid_attached_file :source_video, styles:
                            {
                                trimmed: {
                                    format: "avi",
                                    convert_options: {
                                        output: {
                                            ss: ->(f) { f.from },
                                            to: ->(f) { f.to }
                                        }
                                    }
                                }
                            }, :processors => [:transcoder]

  before_validation :source_video_get_duration
  before_update :reset_state
  validates_attachment_content_type :source_video, :content_type => ["video/mp4", "video/avi", "video/x-msvideo"]
  validates_with ::CustomValidators::VideoDurationValidator
  before_source_video_post_process :before_source_video_processing

  validates :user, presence: true
  validates :source_video, presence: true

  def before_source_video_processing
    enable_processing
  end

  def convert_state
    CONVERT_STATES[super]
  end

  def convert_state=(value)
    return super(CONVERT_STATES.index value) if CONVERT_STATES.include? value
    super
  end

  def enable_processing
    @enable_processing||=false
  end

  private

  def reset_state
    self.convert_state=:new if (changed_attributes.symbolize_keys.keys&[:from, :to, :source_video_fingerprint]).any?
  end

  def source_video_get_duration
    get_video_duration :source_video
  end

  def get_video_duration(field)
    file = self.send(field).queued_for_write[:original].try(:path)
    self.send("#{field}_duration=", Paperclip.run("ffprobe", '-i %s -show_entries format=duration -v quiet -of csv="p=0"' % file).to_f) if file.present?
  end
end
