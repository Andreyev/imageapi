json.extract! video_file, :id, :from, :to, :source_video_duration
json.convert_state I18n.t(video_file.convert_state)
json.original_url video_file.source_video.url
json.trimmed_url video_file.source_video.url(:trimmed)
json.show_url v1_video_file_url(video_file)
json.scedule_url schedule_v1_video_file_url(video_file)
