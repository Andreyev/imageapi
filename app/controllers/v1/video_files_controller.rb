class V1::VideoFilesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_video_file, only: [:show, :edit, :update, :destroy, :schedule]
  after_action :schedule_trimming, only: [:create, :update]

  rescue_from Mongoid::Errors::DocumentNotFound do |e|
    @message = I18n.t(:not_found, id: params[:id])
    @failed_id = params[:id]
    render :not_found, status: 404
  end

  ##
  # Returns list of available video files as array of entities, formatted as described in *show* section
  #
  # url:
  #   /v1/video_files(.:format)
  # named path:
  #   v1_video_files
  # method:
  #   GET
  # parameters:
  #   none
  # required headers:
  #   X-User-Email alice@example.com
  #   X-User-Token 1G8_s7P-V-4MGojaKD7a

  def index
    @video_files = current_user.video_files
    respond_with(:v1, @video_files)
  end
  ##
  # Returns information about particular video file
  #
  # url:
  #   /v1/video_files/:id(.:format)
  # named path:
  #   v1_video_file
  # method:
  #   GET
  # parameters:
  #   none
  # required headers:
  #   X-User-Email alice@example.com
  #   X-User-Token 1G8_s7P-V-4MGojaKD7a
  # examples:
  #   /v1/video_files/unknown_id -> {"message":"Can't found an entity with id unknown_id","id":"unknown_id"}
  #   /v1/video_files/575edcd52aaa5e1f4fc8c7d8 ->
  #   {
  #     "id":{"$oid":"575edcd52aaa5e1f4fc8c7d8"},
  #     "from":80.0,
  #     "to":150.0,
  #     "source_video_duration":289.575,
  #     "convert_state":"File is processed successfully",
  #     "original_url":"/system/video_files/source_videos/575e/dcd5/2aaa/5e1f/4fc8/c7d8/original/%D0%9D%D0%BE%D1%87%D1%8C%D0%BD%D0%BE%D0%B9_%D0%BA%D0%B0%D1%80%D1%82%D0%B8%D0%BD%D0%B3.mp4?1465855390",
  #     "trimmed_url":"/system/video_files/source_videos/575e/dcd5/2aaa/5e1f/4fc8/c7d8/trimmed/%D0%9D%D0%BE%D1%87%D1%8C%D0%BD%D0%BE%D0%B9_%D0%BA%D0%B0%D1%80%D1%82%D0%B8%D0%BD%D0%B3.avi?1465855390",
  #     "show_url":"http://localhost:3000/v1/video_files/575edcd52aaa5e1f4fc8c7d8",
  #     "scedule_url":"http://localhost:3000/v1/video_files/575edcd52aaa5e1f4fc8c7d8/schedule"
  #   }
  #

  def show
    respond_with(:v1, @video_file)
  end

  def new #:nodoc:
    @video_file = current_user.video_files.new
    respond_with(:v1, @video_file)
  end

  def edit #:nodoc:
  end

  ##
  # Schedules processing of the video file, unless it is in processing state
  #
  # url:
  #   /v1/video_files/:id/schedule(.:format)
  # named path:
  #   schedule_v1_video_file
  # method:
  #   POST
  # parameters:
  #   none
  # required headers:
  #   X-User-Email alice@example.com
  #   X-User-Token 1G8_s7P-V-4MGojaKD7a
  # not found error:
  #   If requested ID does not exists, returns json document with status-code 404
  #   {
  #     "message":"Can't found an entity with id unknown_id",
  #     "id":"unknown_id"
  #   }
  # returns:
  #   JSON document similar to result of the show action

  def schedule
    schedule_trimming
    respond_with(:v1, @video_file)
  end

  ##
  # Creates new user video instance and schedules video processing
  #   You should form a valid multipart/form-data request (see RFC 2388)
  #
  # url:
  #   /v1/video_files(.:format)
  # named path:
  #   v1_video_files
  # method:
  #   POST
  # parameters:
  #   from: float - start point to trim the video in seconds
  #   to: float - start point to trim the video in seconds
  #   source_video - attached file
  #   all parameters are required
  # required headers:
  #   X-User-Email alice@example.com
  #   X-User-Token 1G8_s7P-V-4MGojaKD7a
  # returns:
  #   JSON document similar to result of the show action
  # validation errors:
  #   if entered data is not valid, returns json document with status-code 422 and json body like following:
  #     {
  #       "errors":{
  #         "source_video":["can't be blank"]
  #       }
  #     }

  def create
    @video_file = current_user.video_files.new(video_file_params)
    @video_file.save
    respond_with(:v1, @video_file)
  end

  ##
  # Updates an existing video and schedules video processing if updates are successful
  #   You should form a valid multipart/form-data request (see RFC 2388) in order to update an attached file
  #
  # url:
  #   /v1/video_files/:id(.:format)
  # named path:
  #   v1_video_file
  # method:
  #   PATCH, PUT
  # parameters:
  #   from: float - start point to trim the video in seconds
  #   to: float - start point to trim the video in seconds
  #   source_video - attached file
  #   any of these parameters can be omitted
  # required headers:
  #   X-User-Email alice@example.com
  #   X-User-Token 1G8_s7P-V-4MGojaKD7a
  # returns:
  #   JSON document similar to result of the show action
  # not found error:
  #   If requested ID does not exists, returns json document with status-code 404
  #   {
  #     "message":"Can't found an entity with id unknown_id",
  #     "id":"unknown_id"
  #   }
  # validation errors:
  #   if entered data is not valid, returns json document with status-code 422 and json body like following:
  #     {
  #       "errors":{
  #         "source_video":["can't be blank"]
  #       }
  #     }

  def update
    @video_file.update(video_file_params)
    respond_with(:v1, @video_file)
  end

  ##
  # Destroys an existing video file
  #
  # url:
  #   /v1/video_files/:id(.:format)
  # named path:
  #   v1_video_file
  # method:
  #   DELETE
  # parameters:
  #   none
  # required headers:
  #   X-User-Email alice@example.com
  #   X-User-Token 1G8_s7P-V-4MGojaKD7a
  # returns:
  #   Empty document with status-code 204
  # not found error:
  #   If requested ID does not exists, returns json document with status-code 404
  #   {
  #     "message":"Can't found an entity with id unknown_id",
  #     "id":"unknown_id"
  #   }

  def destroy
    @video_file.destroy
    respond_with(:v1, @video_file)
  end

  private

  def schedule_trimming
    unless @video_file.changed? && @video_file.convert_state == :processing
      @video_file.update convert_state: :scheduled
      TrimVideoFileJob.perform_later(@video_file)
    end
  end

  def set_video_file
    @video_file = current_user.video_files.find(params[:id])
  end

  def video_file_params
    params.require(:video_file).permit(:from, :to, :source_video)
  end
end
