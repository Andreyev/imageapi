class TrimVideoFileJob < ActiveJob::Base
  queue_as :trim

  rescue_from(ActiveJob::DeserializationError) do |e|
    logger.error "Deserialization Error!"
    fail
  end

  def perform(*args)
    @video_file = args[0]
    @options = args.extract_options!
    @video_file.enable_processing = true
    @video_file.update! convert_state: :processing
    logger.info "Started video processing"
    begin
      @video_file.source_video.reprocess!
    rescue Exception
      @video_file.update convert_state: :failed
      logger.error "Processing is failed!"
      fail
    end
  end

  after_perform do
    @video_file.update! convert_state: :done
    logger.info "Success!"
  end
end
